package com.devcamp.userorderapi.controllers;

import java.util.Date;
import java.util.List;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.userorderapi.models.Order;
import com.devcamp.userorderapi.models.User;
import com.devcamp.userorderapi.repositorys.OrderRepository;
import com.devcamp.userorderapi.repositorys.UserRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class OrderController {
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    UserRepository userRepository;

    @PostMapping("/order/create/{id}")
    public ResponseEntity<Order> createOrder(@RequestBody Order pOrder, @PathVariable("id") long id) {

        try {
            Optional<User> cUser = userRepository.findById(id);
            if (cUser.isPresent()) {
                Order vOrder = new Order();
                vOrder.setOrderCode(pOrder.getOrderCode());
                vOrder.setPizzaSize(pOrder.getPizzaSize());
                vOrder.setPizzaType(pOrder.getPizzaType());
                vOrder.setCreated_at(new Date());
                vOrder.setPrice(pOrder.getPrice());
                vOrder.setPaid(pOrder.getPaid());
                vOrder.setUser(cUser.get());
                return new ResponseEntity<>(orderRepository.save(vOrder), HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/order/all")
    public List<Order> getAllOrder() {
        return orderRepository.findAll();
    }

    @GetMapping("/order/details/{id}")
    public Order getOrderById(@PathVariable("id") long id) {
        return orderRepository.findById(id).get();
    }

    @PutMapping("/order/update/{id}")
    public ResponseEntity<Order> updateOrder(@RequestBody Order pOrder, @PathVariable("id") long id) {
        Optional<Order> cOrderOptional = orderRepository.findById(id);
        if (cOrderOptional.isPresent()) {
            try {
                cOrderOptional.get().setOrderCode(pOrder.getOrderCode());
                cOrderOptional.get().setPizzaSize(pOrder.getPizzaSize());
                cOrderOptional.get().setPizzaType(pOrder.getPizzaType());
                cOrderOptional.get().setPrice(pOrder.getPrice());
                cOrderOptional.get().setPaid(pOrder.getPaid());
                return new ResponseEntity<>(orderRepository.save(cOrderOptional.get()), HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @DeleteMapping("/order/delete/{id}")
    public ResponseEntity<Order> deleteOrder(@PathVariable("id") long id) {
        orderRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/orderUser/{id}")
    public ResponseEntity<List<Order>> getOrderByIdUder(@PathVariable("id") long id) {
        try {
            List<Order> orders = orderRepository.findByUserId(id);
            return new ResponseEntity<>(orders, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}