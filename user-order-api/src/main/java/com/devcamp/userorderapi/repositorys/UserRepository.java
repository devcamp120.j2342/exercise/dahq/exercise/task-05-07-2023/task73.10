package com.devcamp.userorderapi.repositorys;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.userorderapi.models.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
