package com.devcamp.countryapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "regions")
public class CRegion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "region_code", unique = true)
    private String regionCode;
    @Column(name = "region_name")
    private String regionName;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id", nullable = false)

    private CCountry country;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    // public CCountry getCountry() {
    // return country;
    // }

    public void setCountry(CCountry country) {
        this.country = country;
    }

    public CRegion(long id, String regionCode, String regionName, CCountry country) {
        this.id = id;
        this.regionCode = regionCode;
        this.regionName = regionName;
        this.country = country;
    }

    public CRegion() {
    }

}
