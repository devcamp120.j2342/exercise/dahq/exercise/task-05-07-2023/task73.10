package com.devcamp.cmenuapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.cmenuapi.model.CMenu;

public interface MenuRepository extends JpaRepository<CMenu, Integer> {

}
