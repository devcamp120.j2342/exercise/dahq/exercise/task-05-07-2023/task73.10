package com.devcamp.cmenuapi.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.cmenuapi.model.CMenu;
import com.devcamp.cmenuapi.repository.MenuRepository;
import com.devcamp.cmenuapi.service.MenuService;

@CrossOrigin
@RestController
@RequestMapping("/")

public class MenuController {
    @Autowired
    MenuService menuService;
    @Autowired
    MenuRepository menuRepository;

    @GetMapping("/menu/all")

    public ResponseEntity<List<CMenu>> getAllMenu() {
        return new ResponseEntity<>(menuService.listMenus(), HttpStatus.OK);
    }

    @PostMapping("/menu/create")
    public ResponseEntity<CMenu> createMenu(@RequestBody CMenu pMenu) {
        try {
            return new ResponseEntity<>(menuRepository.save(pMenu), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PutMapping("/menu/update/{id}")
    public ResponseEntity<CMenu> updateMenu(@RequestBody CMenu pMenu, @PathVariable("id") int id) {
        Optional<CMenu> cMenu = menuRepository.findById(id);
        if (cMenu.isPresent()) {
            try {
                cMenu.get().setSize(pMenu.getSize());
                cMenu.get().setDuongKinh(pMenu.getDuongKinh());
                cMenu.get().setNuocNgot(pMenu.getNuocNgot());
                cMenu.get().setSalad(pMenu.getSalad());
                cMenu.get().setSuonNuong(pMenu.getSuonNuong());
                cMenu.get().setThanhTien(pMenu.getThanhTien());
                return new ResponseEntity<>(menuRepository.save(cMenu.get()), HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<CMenu>(HttpStatus.NOT_FOUND);
        }

    }

    @DeleteMapping("/menu/delete/{id}")
    public ResponseEntity<CMenu> deleteMenu(@PathVariable("id") int id) {
        menuRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }

    @GetMapping("/menu/details/{id}")
    public CMenu getMenuById(@PathVariable("id") int id) {
        return menuRepository.findById(id).get();
    }

}
